# https://github.com/ranger/ranger/wiki/Custom-linemodes
from ranger.api.commands import Command

import os
import irdlc

# Save the root directory at the first run
if 'IRROOT' not in globals():
    IRROOT = os.getcwd()
    globals()['IRROOT'] = IRROOT


class irdlcMarkAccept(Command):
    def execute(self):
        paths = [os.path.relpath(f.realpath, IRROOT) for f in self.fm.thistab.get_selection()]
        oldcwd = os.getcwd()
        os.chdir(IRROOT)
        #IR = irdlc.ir.IR(path=os.path.join(IRROOT, 'debian/irdlc.json'))
        IR = irdlc.ir.IR()
        for path in paths:
            IR.set(path, 'accept', '')
        IR.save()
        os.chdir(oldcwd)
        for f in self.fm.thistab.get_selection():
            self.fm.metadata.set_metadata(f.path, {'irdlc_status': "accept"})
        self.fm.ui.redraw_main_column()

class irdlcMarkReject(Command):
    def execute(self):
        paths = [os.path.relpath(f.realpath, IRROOT) for f in self.fm.thistab.get_selection()]
        oldcwd = os.getcwd()
        os.chdir(IRROOT)
        #IR = irdlc.ir.IR(path=os.path.join(IRROOT, 'debian/irdlc.json'))
        IR = irdlc.ir.IR()
        for path in paths:
            IR.set(path, 'reject', '')
        IR.save()
        os.chdir(oldcwd)
        for f in self.fm.thistab.get_selection():
            self.fm.metadata.set_metadata(f.path, {'irdlc_status': "reject"})
        self.fm.ui.redraw_main_column()
