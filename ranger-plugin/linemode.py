# https://github.com/ranger/ranger/wiki/Custom-linemodes
'''
1. put this file under ~/.config/ranger/plugins/
2. add two keybindings in ~/.config/ranger/rc.conf
... TODO
'''

import ranger.api
import ranger.core.linemode

@ranger.api.register_linemode     # It may be used as a decorator too!
class MyLinemode(ranger.core.linemode.LinemodeBase):
    name = "irdlc"

    uses_metadata = True
    required_metadata = ["irdlc_status"]

    def filetitle(self, file, metadata):
        return file.relative_path

    def infostring(self, file, metadata):
        if metadata.irdlc_status == 'accept':
            return "[✓ Accept]"
        else:
            return "[✗ Reject]"
