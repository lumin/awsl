lint:
	-pylint -E -f colorized irdlc/

clean:
	-fdfind __pycache__ -x rm -rf || true

install:
	mkdir -p ~/.local/lib/python3.7/site-packages
	cp -av irdlc ~/.local/lib/python3.7/site-packages
	mkdir -p ~/bin
	cp -av clirdlc ~/bin
