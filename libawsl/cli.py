# Copyright (C) 2020 Mo Zhou <lumin@debian.org>, MIT License
from typing import *
import libawsl.ir as ir
import libawsl.review as rev
import json
#import fire

class Application(object):
    pass

def main(args):
    raise NotImplementedError


def Usage(args):
    print('usage info: ...')
    raise NotImplementedError


def Set(args):
    path = sys.argv[2]
    status = sys.argv[3]
    if len(sys.argv) == 3:
        annotation = ""
    else:
        annotation = " ".join(sys.argv[4:])
    print(f'Set | path={path}, status={status}, annot={annotation}')
    IR = ir.IR()
    IR.set(path, status, annotation)
    IR.save()


def Get(args):
    path = sys.argv[2]
    IR = ir.IR()
    print(json.dumps(IR.get(path), indent=4))


def Accept(args):
    path = sys.argv[2]
    if len(sys.argv) == 2:
        annotation = ""
    else:
        annotation = " ".join(sys.argv[3:])
    print(f'Set | path={path}, status=accept, annot={annotation}')
    IR = ir.IR()
    IR.set(path, 'accept', annotation)
    IR.save()


def Reject(args):
    path = sys.argv[2]
    if len(sys.argv) == 2:
        annotation = ""
    else:
        annotation = " ".join(sys.argv[3:])
    print(f'Set | path={path}, status=reject, annot={annotation}')
    IR = ir.IR()
    IR.set(path, 'reject', annotation)
    IR.save()


def Status(args):
    IR = ir.IR()
    overview, summary = IR.overview()
    for (k, v) in overview.items():
        #print('  ', v, k)
        if v == 'O':
            print('  ', '\x1b[33;1mO\x1b[m', k)
        elif v == 'N':
            print('  ', '\x1b[37mN\x1b[m', k)
        elif v == 'A':
            print('  ', '\x1b[32;1mA\x1b[m', k)
        elif v == 'R':
            print('  ', '\x1b[31;1mR\x1b[m', k)
        else:
            raise ValueError(f"what is {v}?")
    print()
    print(f'Found {summary["ALL"]} files and directories.')
    print(' ', 'Accepted', summary['A'], '; Rejected', summary['R'], '; Outdate', summary['O'], '; N/A', summary['N'])


def Dump(args):
    IR = ir.IR()
    print(json.dumps(IR, indent=2))


def Inspect(args):
    rev.inspect(sys.argv[2])

