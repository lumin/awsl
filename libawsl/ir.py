# Copyright (C) 2020 Mo Zhou <lumin@debian.org>, MIT/Expat License
from collections import namedtuple
import os, sys, re, json
import getpass
import time
import glob
import hashlib


def neighborLicence(path: str) -> str:
    '''
    Return the path of neighboring license file, or empty string.
    '''
    _license = (r'COPYRIGHT.*', r'LICENSE.*')
    dirname = os.path.dirname(path)
    ls = os.listdir(dirname if dirname else '.')
    for name in ls:
        if any(re.match(l, name, re.IGNORECASE) for l in _license):
            return os.path.join(dirname, name)
    if dirname:
        return neighborLicence(dirname)
    else:
        return ""

def saltedHash(path: str) -> object:
    '''
    saltedHash = hash(read(<file>) + read(<license>))
    '''
    hasher = hashlib.md5()
    with open(path, 'rb') as f:
        content = f.read()
    hasher.update(content)
    nlic = neighborLicence(path)
    if nlic:
        with open(nlic, 'rb') as f:
            lcontent = f.read()
        hasher.update(lcontent)
    return hasher.hexdigest()


class _IRStorage(object):
    '''
    Abstract class for IR storage
    '''
    def __init__(self, path: str = None):
        self.path = path
    def save(self, **kwargs):
        raise NotImplementedError
    def load(self, **kwargs):
        raise NotImplementedError
    def update(self, **kwargs):
        raise NotImplementedError
    @staticmethod
    def factory(storage):
        if storage.endswith('.json'):
            return _IRStorage_JSON(storage)
        else:
            raise NotImplementedError



class _IRStorage_JSON(_IRStorage):
    '''
    JSON backend for IR storage
    '''
    defaultpath = 'debian/irdlc.json'

    def save(self, content):
        # use default path if not specified
        if not self.path:
            self.path = self.defaultpath
        # fully dump the data to json file
        with open(self.path, 'wt') as f:
            f.write(json.dumps(content))

    def load(self):
        # use default path if not specified
        if not self.path:
            self.path = self.defaultpath
        # does it exist?
        if not os.path.exists(self.path):
            return {}
        # fully load the json file
        with open(self.path, 'rt') as f:
            j = json.loads(f.read())
        return {k: v for (k, v) in j.items()}


class _IRStorage_SQLITE(_IRStorage):
    pass

class _IRStorage_REDIS(_IRStorage):
    pass

class _IRCore(dict):
    '''
    Core of the Intermediate Reresentation.
    This is a dictionary where the key is a file path, and the
    value is a list of intermediate review results.
    >>> {"./path":
    ...   [
    ...     {
    ...      "time": "1970 Jan 1",
    ...      "hash": 0x1234567890abcdef,
    ...      "user": "john doe",
    ...      "status": "accept",
    ...      "annotation": "example annotation",
    ...     }
    ...   ]
    ... }
    '''
    def set(self, **kwargs):
        raise NotImplementedError

    def get(self, **kwargs):
        '''
        get the results given a path
        :key: str, the file path query
        :history: bool, do you need the full history (default=False)
        '''
        if ('key' not in kwargs):
            raise ValueError("Please specify the 'key' argument")
        if ('history' in kwargs) and kwargs['history']:
            return self[path]
        else:
            return self[path][-1]  # latest result

    def loads(self, content):
        raise NotImplementedError

    def dumps(self, content):
        raise NotImplementedError

    def prune(self):
        raise NotImplementedError


class _IRTree(object):
    def fromCore(self, *args):
        raise NotImplementedError
    def toCore(self, *args):
        raise NotImplementedError


class IR(dict):
    '''
    wrapper of _IRStorage and _IRCore, plus some sanity checks
    '''
    def __init__(self, *, storage: str = 'debian/irdlc.json'):
        super(IR, self).__init__()
        self.storage = _IRStorage.factory(storage)
        self.core = _IRCore()
        self.load()

    def get(self, path: str, *, history=False):
        '''
        get the results given a path
        '''
        if history:
            return self[path]
        else:
            return self[path][-1]

    def set(self, path: str, *args):
        if len(args) == 0:
            raise ValueError("missing expected values")
        elif len(args) > 2:
            raise ValueError("too many arguments")
        tm = time.time()
        shash = saltedHash(path)
        user = getpass.getuser()
        status = args[0]
        if status not in ('accept', 'reject'):
            raise ValueError('status must be either accept or reject')
        anno = args[1] if len(args)>0 else ""
        if path not in self.keys():
            self[path] = []
        self[path].append({'time': tm, 'hash': shash, 'user': user,
            'status': status, 'annotation': anno})

    def save(self, dest: str = ''):
        self.storage.save(self)

    def load(self, src: str = ''):
        j = self.storage.load()
        for (k, v) in j.items():
            self[k] = v

    def overview(self) -> list:
        flist = glob.glob('**', recursive=True)
        flist = [x for x in flist if not re.match(r'\.git.*', x)]
        # generate result for plain files
        result = {x: 'N' for x in flist}
        for (k, hs) in self.items():
            if k not in result:
                continue
            if saltedHash(k) == hs[-1]['hash']:
                result[k] = hs[-1]['status']
                result[k] = 'A' if result[k] == 'accept' else 'R'
            else:
                result[k] = 'O'
        # generate summary
        summary = {x: 0 for x in 'ARON'}
        for (k, v) in result.items():
            summary[v] = summary[v] + 1
        summary['ALL'] = len(result)
        return result, summary

