# 2019 (C) Mo Zhou <lumin@debian.org>, MIT License
from typing import *
import re
import json
from . import ir

class AbstractReviewer(object):
    """
    input[str]: file path
    """
    def scan(self, src: str) -> Dict:
        raise NotImplementedError

class Grep(AbstractReviewer):
    def __init__(self):
        self.name = 'Grep'
    def scan(self, src: str) -> Dict:
        with open(src, 'r') as f:
            lines = f.readlines()
        results = []
        for line in lines:
            if re.match('.*copyright.*', line, re.IGNORECASE):
                results.append(line.strip())
        return {'result': results}

class DCopyright(AbstractReviewer):
    pass

class Neighbor(AbstractReviewer):
    pass

class Licensecheck(AbstractReviewer):
    pass

def getchar():
    #Returns a single character from standard input
    import tty, termios, sys
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    try:
        tty.setraw(sys.stdin.fileno())
        ch = sys.stdin.read(1)
    finally:
        termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
    return ch

def inspect(path: str):
    '''
    (interative)
    '''
    IR = ir.IR()
    reviewers = [Grep()]
    print('\x1b[46;1mFile', '|', path, '\x1b[m')
    for reviewer in reviewers:
        res = reviewer.scan(path)
        print(reviewer.name, '\x1b[32;1m>\x1b[m', json.dumps(res))
