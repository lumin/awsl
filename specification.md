LibAWSL Specification
=====================

LibAWSL Specification: Improving Human Efficiency for debian/copyright Review

```
Copyright (C) 2019-2020, Mo Zhou <lumin@debian.org>, CC-BY-SA-4.0 License
LastUpdate: May 24 2020
SpecVersion: 1.0.2
```

LibAWSL specifications aims to shed light on the software/workflow designing
issue on helping debian/copyright reviewers to improve their efficiency. This
optimization could be significantly meaningful, as the pace of the whole
distribution can be boosted as long as the NEW queue process becomes faster.

**Disclaimer**: Some points in this specification, sometimes radical, may
contradict with our current practice. This is a specification for THEORETICAL
DISCUSSION, instead of proposing to enforce something. Please DON'T
misunderstand that.

**SideNote**: [Trivial] "AWSL" is a Chinese meme. I chose it for no reason
since I'm terrible at picking project names.

The Process to be Optimized
---------------------------

* Given a debian-format source package (which includes the upstream source and
  a `debian/` directory), the reviewer checks every source file (text and
binary) and its corresponding information in `debian/copyright`, and confirms
wether the whole source package is DFSG-compliant. (Input = A source package;
Output = A boolean value, i.e.g ACCEPT/REJECT).

Let's call this problem as the **"srcpkg-bool"** problem.

Proposed Principles in Human-Understandable Language
----------------------------------------------------

* The reviewer does not have to review the IDENTICAL file more than once.

* The time complexity for going through a package should be less or equal to
  O(num-of-files).

They will be explained in detail in the next section.

Proposed Principles in Detail
-----------------------------

Let's first decompose the **"srcpkg-bool"** problem into a series of
subproblems organized in a tree structure, where each subproblem is called
**"file-bool"** problem. The "file-bool" problem is defined as: given an
upstream source file (either binary or plain text) and a `debian/copyright`
file, the reviewer decides either to `ACCEPT` or `REJECT` the file according to
Debian Policy and DFSG. When all the files under a certain subdirectory
(subtree) are "ACCEPTED", we automatically "ACCEPT" the subtree. Recursively,
when all the subtrees under the root tree have been "ACCEPTED", the source
package is deemed as "ACCEPTED".  In this way, the coarse-grained
**"srcpkg-bool"** problem is decomposed into **"file-bool"** subproblems
organized in a tree structure, homogenious to the directory tree.

At the same time, we need to define a new data structure to record the results
of the "file-bool" subproblems. Each source package corresponds to a dictionary
(hash table), where the keys of the dictionary are the file paths, and the
values a list of metadata. The metadata is a tuple consisting of the following
elements:

* **`timestamp`** (string): when this metadata is created.

* **`saltedHash`** (string): hash(filecontent, copyright-info, neighbor-license),
  where the "filecontent" is literally the file content. "copyright-info"
  is the corresponding "Copyright:" and "License:" information recorded in
  `debian/copyright`. "neighbor-license" is the `LICENSE`- or `COPYING`- file
  content near the file in the directory hierarchy (just like how `git` looks
  for the `.git` directory).

* **`username`** (string): who created this metadata.

* **`decision`** (bool): ACCEPT / REJECT.

* **`annotation`** (string): optional string for explaining why the `decision` has
  been made. Empty string is the default for this element.

Given such a data structure (I used to call it sort of "intermediate
representation" but actually I should not scare people off using unfriendly
words), we can build a concrete impression on how the "srcpkg-bool" problem
can be decomposed into subproblems in a certain software implementation.

* When `dict["path/to/file"][all] == None`, it means no one has reviewed this
  file, and the review status is "N/A".
* When `dict["path/to/file"][latest].saltedHash` mismatches with the latest
  salted hash, suggesting that either the content of the source file or the
  related copyright info has been changed, it means the review status of this
  file is "outdated", or say "needs to be reviewed again".
* When `dict["path/to/file"][latest].saltedHash` matches with the latest one,
  we can reuse the latest `decision` without reviewing the file `path/to/file`
  again.
* The review status of a subtree is "N/A" when the status of any decendant
  child node is in "N/A". Namely, the "N/A" status of any file contaminates
  the status of the whole tree.
* The review status of a subtree is "REJECT", when there is no unreviewed
  file, but one or more "REJECTED" files. Namely, "REJECT" status of any
  file taints the review status of the whole tree, if "N/A" does not apply.
* The review status of a subtree is "ACCEPT" if and only if the review status
  of all decendant nodes (incl. files, subtrees) are unexceptionally "ACCEPT".

In this way, we can solve the `srcpkg-bool` problem via the `file-bool`
surrogate problems.

Finally we can look into the *two proposed principles*.

* The reviewer does not have to review the IDENTICAL file more than once.
  (Let's call it the "Deduplication Priciple")

**Explanation**: We compute `saltedHash` to assess the validity of the latest
metadata for a given file. When none of (1) the file content, (2) the
corresponding "Copyright:" info and "License:" info, (3) the adjacent
`LICENSE`- or `COPYRING`-alike file, has been changed, we should reuse the
latest decision (i.e. ACCEPT/REJECT), because we should NOT review an identical
file for more than once.

**Benefit**: Definite reduction in time consumption for processing a
`srcpkg-bool` problem. For example, when a new upstream release has only
changed one file, the reviewer only have to process a single `file-bool`
problem instead of processing the whole `srcpkg-bool` problem from scratch.

* The time complexity for going through a package should be less or equal to
  O(num-of-files). (Let's call it the "Linearization Principle")

**Explanation**: when processing a `srcpkg-bool` problem, one may open several
terminals, where one displays the content of `debian/copyright`, while another
displays a terminal-based file browser. It must be noted that the time
complexity for such processing task is not linear -- the time consumption is
not proportional to the number of files. One will spend huge amount of time in
(1) seeking for the next file to review; (2) grep the possible copyright
information from the file; (3) check the adjacent `LICENSE` file when no
copyright information is present in the current file; (4) seeking in
d/copyright back and forth for the corresponding "Copyright:" and "License:"
info. Particularly, the time consumption in the (4) point is non-linear: when
the `d/copyright` file gets larger and longer, it will costs the reviewer more
time to browse back and forth for the desired information. Hence, the time
consumption for processing a `srcpkg-bool` issue is nonlinear to the number of
files. **Solution:** When reviewing a certain file, a sane helper tool should
print the following information on the same terminal at the same time: (1)
`grep -i copyright <file>` or the file header; (2) `licensecheck <file>`; (3)
`licensecheck <adjacent-license-file>`; (4) "Copyright:" and "License:"
information from `debian/copyright` corresponding to the file to be reviewed.

**Benefit**: The reviewer will be able to do the review with a single terminal.
Time consumption in (1), (2), (3), (4) can be eliminated by using the machine's
processing power. Hopefully we can make the time consumption for a whole
package linear to the number of files to be reviewed. Besides, a "Batch Mode"
for marking a batch of files as "ACCEPT/REJECT" can further reduce the time
consumption, but let's leave the detail in future discussion.

How Can We Benifit from LibAWSL in Practice
-------------------------------------------

* Faster NEW queue processing speed, hence higher efficiency

**Explanation**: The "Deduplicate Principle" eliminated time cost on duplicated
review works. The "Linearization Principle" eliminated time cost on some
annoying seeking and navigating steps. Compared the the status quo, these
automations means faster speed and higher efficiency. Besides, the reviewer
experience will possibly be improved as well since we have eliminated some
boring parts.

* Instant acceptance for source packages with merely binary package rename
  (without change in upstream code)

**Explanation**: We often confront with situations where we have to add or
rename some binary packages without any upstream source change. With the
specification, we can reuse the previous decision of the whole file tree
because none of the `saltedHash` of the files has been changed. Packages in
this case should be able to pass the NEW queue instantly. Eliminating
duplicated reviews really reduces the COST of our community.

* More verbose and structured feedback from ftp team

**Explanation**: We can easily dump the ftp team feedback from the tree-shaped
data structure, by printing the REJECTED file nodes: `file -> annotation` where
the `annotation` element in the metadata is exactly the reason of rejection.

* Accumulation of precious educational resources, and reducing training cost
  for ftp-trainees

**Explanation**: We don't have enough document for training a ftp-trainee.
Newbies do not know how to deal with corner cases (e.g. CDDL v.s. GPL) at all,
and asking ftp-masters to write down all the corner cases is definitely
impractical.  However, by grepping from the history REJECTION notes, not only
new ftp trainees but also ordinary debian developers could learn from them how
to deal with special cases. I personally think this could be a precious
educational resource, that can be implicitly accumualted from ftp-master's
work.

* Convenient new-upstream-release checks for maintainers and reviewers

**Explanation**: When there are upstream code changes, we only have to check
the changed files (with review status "outdated"), and we can reuse the the
review status of the rest, unchagned files.

* Making the reviewing process interruptable

**Explanation**: When processing a `srcpkg-bool` problem for a giant source
package containing millions of files, the review must get mentally prepared and
pre-allocate a contiguous chunk of time to do the task. Once interrupted, the
reviewer is very likely to forget: "where is the interrupt point of my work?",
"which are the questionable files I have found before?", "what was I going to
prod the maintainer about?". With the proposed specification, the reviewing
process can be resistent enough against the negative effects of interruption,
because the review status of any file or subtree can be calculated again by the
machine at any time, telling the reviewer "which files are still not reviewed
(e.g. N/A, or outdated) yet", "which are the files in question with my
annotation?", etc.

* Possibility for open/collaborated reviewing workflow

**Explanation**: After breaking a `srcpkg-bool` issue down into a series of
INDEPDENDENT `file-bool` subproblems, we can directly apply the idea of the
MapReduce algorithm to distribute the workload into a batch of reviewers, hence
enabling the possibility of collaborate or even open review.

For sake of a more efficient Debian Community.


Discussions
-----------

1. Should the "N/A" status of a node dominate the "REJECT" status when
   evaluating the review status of a subtree, or vise versa? Currently the
   "N/A" is prior to "REJECT". I think letting "REJECT" override "N/A" will
   encourage the reviewer to do incomplete reviews, hence a possibly ping-pong
   game between submitters and reviewers.

2. 'seeking in d/copyright back and forth for the corresponding "Copyright:"
   and "License:" info' -- I don't know whether we have a tool for this.

3. Is the amount of unchanged files during upgrade really larger than that
   of changed files? I don't know. We can do some statistics with the help
   of snapshot.d.o.

See Also
--------

* [A proposal to resolve long processing delays in the FTP-Masters NEW queue.](https://salsa.debian.org/mtecknology/ftpmaster-proposal)

The above reference, highly related to this specification, focuses on problems
in a larger scale compared to this specification. This specification is
dedicated to discuss only a SINGLE well-defined problem.
