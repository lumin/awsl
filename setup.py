#!/usr/bin/python3
import setuptools

setuptools.setup(
        name="awsl",
        version='0.0.1',
        author="Mo Zhou",
        author_email="lumin@debian.org",
        description="debian/copyright reviewer helper",
        packages=setuptools.find_packages(),
        )
